package zexos.com.ifoto.ifoto.controllers;

import lombok.Data;

@Data
class AddRole {
    private String username;
    private String role;
}