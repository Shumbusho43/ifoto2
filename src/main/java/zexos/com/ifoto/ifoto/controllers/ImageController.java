package zexos.com.ifoto.ifoto.controllers;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import zexos.com.ifoto.ifoto.exceptions.BadRequestException;
import zexos.com.ifoto.ifoto.models.AppUser;
import zexos.com.ifoto.ifoto.models.Images;
import zexos.com.ifoto.ifoto.payload.ImageResponseObject;
import zexos.com.ifoto.ifoto.repository.UserRepository;
import zexos.com.ifoto.ifoto.service.ImageService;
import zexos.com.ifoto.ifoto.utils.Response;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@Slf4j
@AllArgsConstructor
@NoArgsConstructor
public class ImageController {
    @Autowired
    private ImageService imageService;
    private UserRepository userRepository;
//    @PostMapping( "/upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
@RequestMapping(value="/upload", method=RequestMethod.POST,consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<Response> uploadImage(@RequestPart MultipartFile file,@RequestPart UUID userId) throws IOException {
    Optional<AppUser> user=userRepository.findById(userId);
    System.out.println(userId);
    if (user==null){
        throw new BadRequestException("User not found");
    }
    imageService.store(file,userId);
            return ResponseEntity.status(HttpStatus.OK).body(new Response(true, "Image Uploaded Successfully", ZonedDateTime.now()));
    }
    @GetMapping("/image/{imageId}")
    public ResponseEntity<byte[]> getImage(@PathVariable("imageId") UUID imageId) {
        try {
            Images image= imageService.getImage(imageId);
           return ResponseEntity.ok()
                   .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + image.getName() + " \"")
                   .body(image.getData());
        }
        catch (Exception e) {
            log.error("Exception Occurred: ", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }
    @GetMapping("/images")
    public  ResponseEntity<List<ImageResponseObject>> getAllImages() {
        try {
            List<ImageResponseObject> images = imageService.getAllImages().map(image -> {
                String fileDownloadUri = ServletUriComponentsBuilder
                        .fromCurrentContextPath()
                        .path("/image/")
                        .path(image.getId().toString())
                        .toUriString();
                return new ImageResponseObject(image.getName(),fileDownloadUri, image.getType(),image.getData().length);
                    }).collect(Collectors.toList());
            return ResponseEntity.status(HttpStatus.OK).body(images);
        }
        catch (Exception e) {
            log.error("Exception Occurred: ", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }
}
