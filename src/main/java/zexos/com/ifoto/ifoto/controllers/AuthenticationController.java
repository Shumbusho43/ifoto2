package zexos.com.ifoto.ifoto.controllers;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import zexos.com.ifoto.ifoto.dtos.SignInDto;
import zexos.com.ifoto.ifoto.repository.RoleRepository;
import zexos.com.ifoto.ifoto.repository.UserRepository;
import zexos.com.ifoto.ifoto.service.MailService;
import zexos.com.ifoto.ifoto.service.UserService;

import java.util.List;

@RestController
@Slf4j
public class AuthenticationController {
    @PostMapping("/login")
    @ApiOperation(value = "Sign in to u account")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful operation",
                    responseHeaders = {@ResponseHeader(name = "Authorization", description = "Bearer <JWT here>", response = String.class)})
    })
    public void fakeLogin(@RequestParam String username, @RequestParam String password) {
        throw new IllegalStateException("This method shouldn't be called, it's implemented by the Spring Security");
    }
}
//
//@RestController
//@RequestMapping(path = "/api/v1/auth")
//public class AuthenticationController {
//    @Autowired
//    private UserService userService;
//
//    @Autowired
//    private AuthenticationManager authenticationManager;
//
//    @Autowired
//    private JwtTokenProvider jwtTokenProvider;
//
//    @Autowired
//    private RoleRepository roleRepository;
//
//
//    @Autowired
//    private BCryptPasswordEncoder bCryptPasswordEncoder;
//
//
//    @Autowired
//    private UserRepository userRepository;
//
//    @Autowired
//    private MailService mailService;
//    @Autowired
//    private Auth auth;
//
//    //    Function to check for incoming email
//    public Boolean checkAtInEmail(String incomingEmail) {
//        int atTracker = 0;
//        for (int i = 0; i < incomingEmail.length(); i++) {
//            if (incomingEmail.charAt(i) == '@') {
//                atTracker = 1;
//            }
//        }
//
//        if (atTracker == 1) {
//            return true;
//        }
//        return false;
//    }
//
//    @PostMapping(path = "/signin")
//    @ApiOperation(value = "signin into your account")
//    public ResponseEntity<JwtAuthenticationResponse> signin(@Valid @RequestBody SignInDto signInRequest) {
//        String signingEmail = signInRequest.getUsername();
//        //        if(!auth.checkUserStatus(signInRequest.getEmail())){
//        if (!auth.checkUserStatus(signingEmail)) {
//            throw new ApiRequestException("Your account is not active, please contact your administrator.");
//        }
//        Authentication authentication = authenticationManager.authenticate(
//                new UsernamePasswordAuthenticationToken(
//                        signingEmail,
//                        signInRequest.getPassword()));
//
//        SecurityContextHolder.getContext().setAuthentication(authentication);
//
//        String jwt = null;
//
//        try {
//            jwt = jwtTokenProvider.generateToken(authentication);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        return ResponseEntity.ok(new JwtAuthenticationResponse(jwt));
//    }
//}

