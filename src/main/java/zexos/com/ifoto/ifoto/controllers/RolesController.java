package zexos.com.ifoto.ifoto.controllers;

import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import zexos.com.ifoto.ifoto.dtos.RoleRegDto;
import zexos.com.ifoto.ifoto.models.Roles;
import zexos.com.ifoto.ifoto.repository.RoleRepository;
import zexos.com.ifoto.ifoto.service.RoleService;
import zexos.com.ifoto.ifoto.utils.Response;

import java.time.ZonedDateTime;

@RestController
@RequestMapping("/api/v1/roles")
@AllArgsConstructor
public class RolesController {
    @Autowired
    private final RoleService roleService;
    @Autowired
    RoleRepository roleRepository;
    //saving new role in the database
    @ApiOperation(value = "Saving new role in the database",notes = "Saving new role in the database")
    @PostMapping("/save")
    public ResponseEntity<Response> saveRole(@RequestBody RoleRegDto role) {
        Roles roles = new Roles(role.getRoleName(),null);
        if (!(role.getRoleName().equals("ROLE_ADMIN") || role.getRoleName().equals("ROLE_USER"))) {
            return new ResponseEntity<>(new Response(false, "Role " + role.getRoleName() + " not set", ZonedDateTime.now()), HttpStatus.BAD_REQUEST);
        } else {
            Roles roleExist = roleRepository.findByName(role.getRoleName());
            if (roleExist != null) {
                return new ResponseEntity<>(new Response(false, "Role " + role.getRoleName() + " already exist", ZonedDateTime.now()), HttpStatus.BAD_REQUEST);
            }
            roleService.saveRole(roles);
            return new ResponseEntity<>(new Response(true, "Role " + role.getRoleName() + " saved", ZonedDateTime.now()), HttpStatus.CREATED);
        }
    }
}
