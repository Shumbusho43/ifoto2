package zexos.com.ifoto.ifoto.controllers;

import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import zexos.com.ifoto.ifoto.dtos.*;
import zexos.com.ifoto.ifoto.enums.ErrorMessages;
import zexos.com.ifoto.ifoto.exceptions.BadRequestException;
import zexos.com.ifoto.ifoto.models.AppUser;
import zexos.com.ifoto.ifoto.models.Roles;
import zexos.com.ifoto.ifoto.payload.UserResponseObject;
import zexos.com.ifoto.ifoto.repository.RoleRepository;
import zexos.com.ifoto.ifoto.repository.UserRepository;
import zexos.com.ifoto.ifoto.service.MailService;
import zexos.com.ifoto.ifoto.service.UserService;
import zexos.com.ifoto.ifoto.utils.CalculateCurrentTime;
import zexos.com.ifoto.ifoto.utils.Response;
import zexos.com.ifoto.ifoto.utils.Utility;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.UUID;

@RestController
@AllArgsConstructor
@Slf4j
@Transactional
@RequestMapping("/api/v1/users")
//@CrossOrigin(origins = "http://localhost:4200")
public class UserController {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private MailService mailService;
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    //getting all users;
    @ApiOperation(value = "Get all users")
    @GetMapping("/all")
    public Page<AppUser> getUsers(int page,int size) {
        //setting default page number and size
        if (page == 1) {
            page = 0;
        }
        else if(page==0)
        {
            page=0;
        }
        else {
            page = page - 1;
        }
        return userService.getUsers(page, size);
    }
    //saving user into database
    @ApiOperation(value = "Create new user",notes = "Create new user")
    @PostMapping()
    public ResponseEntity<Response> saveUser(@RequestBody @Valid UserSignUpDto userSignUpDto){
        AppUser appUser = new AppUser(userSignUpDto.getUsername(),userSignUpDto.getPassword(),false, CalculateCurrentTime.getCurrentTimeUsingDate("yyyy-MM-dd HH:mm:ss"));
        //finding if user exist
       AppUser userExist=userRepository.findByUsername(appUser.getUsername());
       if(userSignUpDto.getUsername()==null || userSignUpDto.getPassword()==null) {
           throw new BadRequestException(ErrorMessages.MISSING_REQUIRED_FIELD.getMessage());
       }
       //checking for valid email address
       if(!userSignUpDto.getUsername().matches("^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$")){
           throw new BadRequestException(ErrorMessages.INVALID_EMAIL_FORMAT.getMessage());
       }
        if(userExist!=null){
            throw new BadRequestException(ErrorMessages.RECORD_EXISTS.getMessage());
        }
       userService.addUser(appUser);
        //sending otp
        appUser.setActivationCode(Utility.randomUUID(6,0,'N'));
        userRepository.save(appUser);
        mailService.sendOptCode(appUser.getUsername(),appUser.getActivationCode());
        return new ResponseEntity(new Response(true,"Activation code was sent to "+userSignUpDto.getUsername(), ZonedDateTime.now()),HttpStatus.CREATED);
    }
    //activating account
    @PutMapping("/activate-account/{code}")
    public ResponseEntity<Response> activateAccount(@PathVariable("code")String code){
        AppUser user=userRepository.findByActivationCode(code.trim());
        if(user==null){
            throw new BadRequestException(ErrorMessages.INVALID_ACTIVATION_CODE.getMessage());
        }
        user.setActivationCode(null);
        user.setVerified(true);
        String time=CalculateCurrentTime.getCurrentTimeUsingDate("yyyy-MM-dd HH:mm:ss");
        user.setActivatedAt(time);
        user.setStatus("ACTIVE");

        userRepository.save(user);
        return new ResponseEntity(new Response(true,"Account activated successfully", ZonedDateTime.now()),HttpStatus.OK);
    }
    //assigning role to user
    @ApiOperation(value = "Assign role to user",notes = "Assign role to user")
    @PostMapping("/addRole")
    public ResponseEntity<Response> addRole(@RequestBody AddRole form) {
        //finding if role exist
        Roles roleExist=roleRepository.findByName(form.getRole());
        if(roleExist==null){
            return new ResponseEntity(new Response(false,"Role was not found", ZonedDateTime.now()),HttpStatus.BAD_REQUEST);
        }
        AppUser userExist=userRepository.findByUsername(form.getUsername());
        if(userExist==null){
            return new ResponseEntity(new Response(false,"User was not found", ZonedDateTime.now()),HttpStatus.BAD_REQUEST);
        }
        userService.addRoleToUser(form.getUsername(), form.getRole());
        return new ResponseEntity(new Response(true,"Role was added", ZonedDateTime.now()),HttpStatus.CREATED);
    }
    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/{email}")
    public ResponseEntity<Response> getUserById(@PathVariable("id") String email) {
        ModelMapper modelMapper=new ModelMapper();
        AppUser user=userService.getUser(email);
        UserResponseObject userResponseObject=modelMapper.map(user,UserResponseObject.class);
        return new ResponseEntity(new Response(true,"User was found",userResponseObject, ZonedDateTime.now()),HttpStatus.OK);
    }
    @DeleteMapping
    public ResponseEntity<Response> deleteUser(){
        AppUser user=userService.getLoggedInUser();
        userService.deleteUser(user);
        return new ResponseEntity(new Response(true,"User was deleted", ZonedDateTime.now()),HttpStatus.OK);
    }
    @GetMapping(path = "/current")
    public ResponseEntity<Response> currentlyLoggedInUser() {
        return ResponseEntity.ok(new Response(true, "user found", userService.getLoggedInUser(), ZonedDateTime.now()));
    }
    @ApiOperation(value = "Reset account password")
    @PostMapping("/initiate-reset-password")
    public ResponseEntity<Response> resetPassword(@RequestBody ResetPasswordDto resetPasswordDto) throws  Exception {
        try {
        AppUser user = userRepository.findByUsername(resetPasswordDto.getEmail());
        if (user == null) {
            throw new BadRequestException(ErrorMessages.USER_NOT_FOUND.getMessage());
        }
        user.setActivationCode(Utility.randomUUID(6,0,'N'));
        userRepository.save(user);
        mailService.sendResetPasswordMail(user.getUsername(),user.getActivationCode());
        return ResponseEntity.ok(new Response(true, "Please check your mail for reset password code", ZonedDateTime.now()));
        }
        catch (Exception e){
            return ResponseEntity.ok(new Response(false, e.getMessage(), ZonedDateTime.now()));
        }
    }
    @PutMapping(path = "/reset-password")
    @ApiOperation(value = "Change your password")
    public ResponseEntity<Response> confirmPassword(@RequestBody @Valid ResetPasswordRequest  resetPasswordRequest){
     AppUser user = userRepository.findByUsername(resetPasswordRequest.getEmail());
     if(user == null){
         throw  new BadRequestException(ErrorMessages.USER_NOT_FOUND.getMessage());
     }
     if(!resetPasswordRequest.getPassword().equals(resetPasswordRequest.getConfirmPassword())){
         throw  new BadRequestException(ErrorMessages.PASSWORD_NOT_MATCH.getMessage());
     }
     if(Utility.isCodeValid(user.getActivationCode(),resetPasswordRequest.getActivationCode())){
         user.setPassword(bCryptPasswordEncoder.encode(resetPasswordRequest.getPassword()));
         user.setActivationCode(null);
         userRepository.save(user);
     }else{
         throw new BadRequestException("Invalid code");
     }
     return ResponseEntity.ok(new Response(true,"Password reset went well!!",ZonedDateTime.now()));
    }
    @PutMapping(path = "/change-password")
    @ApiOperation(value = "Change your password")
    public ResponseEntity<Response> changePassword(@RequestBody @Valid ChangePasswordRequest changePasswordRequest){
        AppUser user = userRepository.findByUsername(changePasswordRequest.getEmail());
        if(user == null){
            throw  new BadRequestException(ErrorMessages.USER_NOT_FOUND.getMessage());
        }
        if(!changePasswordRequest.getNewPassword().equals(changePasswordRequest.getConfirmPassword())){
            throw  new BadRequestException(ErrorMessages.PASSWORD_NOT_MATCH.getMessage());
        }
        if(bCryptPasswordEncoder.matches(changePasswordRequest.getOldPassword(),user.getPassword())){
            user.setPassword(bCryptPasswordEncoder.encode(changePasswordRequest.getNewPassword()));
            userRepository.save(user);
        }else{
            throw new BadRequestException("Invalid old password");
        }
        return ResponseEntity.ok(new Response(true,"Password changed successfully",ZonedDateTime.now()));
    }
    @PostMapping(path = "/initiate-change-email")
    @ApiOperation(value = "Change your email")
    public ResponseEntity<Response> changeEmail(@RequestBody @Valid ChangeEmailRequest changeEmailRequest){
        AppUser user = userRepository.findByUsername(changeEmailRequest.getOldEmail());
        if(user == null){
            throw  new BadRequestException(ErrorMessages.USER_NOT_FOUND.getMessage());
        }
        //sending code to new email
        user.setActivationCode(Utility.randomUUID(6,0,'N'));
        user.setUsername(changeEmailRequest.getNewEmail());
        user.setStatus("PENDING");
        user.setVerified(false);
        userRepository.save(user);
        mailService.sendChangeEmailMail(changeEmailRequest.getNewEmail(),user.getActivationCode());
      return ResponseEntity.ok(new Response(true,"We sent a verification code to your new email "+changeEmailRequest.getNewEmail(),ZonedDateTime.now()));
    }
    @PutMapping(path = "/verify-new-email")
    @ApiOperation(value = "Verify your new email")
    public ResponseEntity<Response> verifyEmail(@RequestBody @Valid VerifyNewEmailRequest verifyEmailRequest){
        AppUser user = userRepository.findByActivationCode(verifyEmailRequest.getVerificationCode());
        if(user == null){
            throw  new BadRequestException(ErrorMessages.INVALID_ACTIVATION_CODE.getMessage());
        }
        if(Utility.isCodeValid(user.getActivationCode(),verifyEmailRequest.getVerificationCode())){
            user.setStatus("ACTIVE");
            user.setVerified(true);
            user.setActivationCode(null);
            userRepository.save(user);
        }else{
            throw new BadRequestException("Invalid code");
        }
        return ResponseEntity.ok(new Response(true,"Email verified successfully",ZonedDateTime.now()));
    }
    //uploading profile picture
    @RequestMapping(value="/upload_profile", method=RequestMethod.POST,consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<Response> uploadImage(@RequestPart MultipartFile file,@RequestParam UUID userId) throws IOException {
        userService.uploadProfilePicture(file,userId);
        return ResponseEntity.status(HttpStatus.OK).body(new Response(true, "Image Uploaded Successfully", ZonedDateTime.now()));
    }
}
