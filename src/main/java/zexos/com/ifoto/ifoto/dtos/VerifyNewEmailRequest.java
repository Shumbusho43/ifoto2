package zexos.com.ifoto.ifoto.dtos;

import lombok.Data;

@Data
public class VerifyNewEmailRequest {
    String verificationCode;
}
