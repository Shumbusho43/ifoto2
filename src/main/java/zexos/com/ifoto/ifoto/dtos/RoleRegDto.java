package zexos.com.ifoto.ifoto.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
public class RoleRegDto {
    private String roleName;
}
