package zexos.com.ifoto.ifoto.dtos;

import lombok.Data;

@Data
public class ChangeEmailRequest {
    private String oldEmail;
    private String newEmail;
}
