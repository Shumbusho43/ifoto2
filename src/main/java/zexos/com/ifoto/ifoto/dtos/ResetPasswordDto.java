package zexos.com.ifoto.ifoto.dtos;

import lombok.Data;

@Data
public class ResetPasswordDto {
    private String email;
}
