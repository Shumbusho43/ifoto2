package zexos.com.ifoto.ifoto.dtos;

import lombok.Data;

@Data
public class SignInDto {
    private String username;
    private String password;
}
