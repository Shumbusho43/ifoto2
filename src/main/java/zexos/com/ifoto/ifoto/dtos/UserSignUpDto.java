package zexos.com.ifoto.ifoto.dtos;

import lombok.Data;

@Data
public class UserSignUpDto {
    private String username;
    private String password;
}
