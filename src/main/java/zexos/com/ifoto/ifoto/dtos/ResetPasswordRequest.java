package zexos.com.ifoto.ifoto.dtos;

import lombok.Data;

@Data
public class ResetPasswordRequest {
    //reset password
    private String email;
    private String password;
    private String confirmPassword;
    private String activationCode;
}
