package zexos.com.ifoto.ifoto.exceptions;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import zexos.com.ifoto.ifoto.utils.ErrorResponse;

import java.util.Date;

@ControllerAdvice
public class AppException {
    @ExceptionHandler(value = {BadRequestException.class})
    public ResponseEntity<Object> handleBadRequestException(BadRequestException ex, WebRequest request) {
        ErrorResponse error = new ErrorResponse(new Date(),false, HttpStatus.BAD_REQUEST.value(), ex.getMessage());
        return new ResponseEntity<>(error, new HttpHeaders(), HttpStatus.BAD_REQUEST.value());
    }
    @ExceptionHandler(value = {MaxUploadSizeExceededException.class})
    public ResponseEntity<Object> handleMaxUploadSizeExceededException(MaxUploadSizeExceededException ex, WebRequest request) {
        ErrorResponse error = new ErrorResponse(new Date(),false,HttpStatus.BAD_REQUEST.value(), "File is too large");
        return new ResponseEntity<>(error, new HttpHeaders(), HttpStatus.BAD_REQUEST.value());
    }
    //handle other exceptions
    @ExceptionHandler(value = {Exception.class})
    public ResponseEntity<Object> handleOtherException(Exception ex, WebRequest request) {
        ErrorResponse error = new ErrorResponse(new Date(),false,HttpStatus.INTERNAL_SERVER_ERROR.value(), ex.getMessage());
        return new ResponseEntity<>(error, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR.value());
    }
}
