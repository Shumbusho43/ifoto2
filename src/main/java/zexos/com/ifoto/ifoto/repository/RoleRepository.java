package zexos.com.ifoto.ifoto.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zexos.com.ifoto.ifoto.models.Roles;

import java.util.UUID;
@Repository
public interface RoleRepository extends JpaRepository<Roles, UUID> {
    Roles findByName(String name);
}
