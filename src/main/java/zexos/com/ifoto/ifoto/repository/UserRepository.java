package zexos.com.ifoto.ifoto.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zexos.com.ifoto.ifoto.models.AppUser;

import java.util.UUID;

@Repository
public interface UserRepository extends JpaRepository<AppUser, UUID> {
    AppUser findByUsername(String username);
    AppUser findByActivationCode(String code);
}
