package zexos.com.ifoto.ifoto.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zexos.com.ifoto.ifoto.models.Images;

import java.util.UUID;
@Repository
public interface ImageRepostory extends JpaRepository<Images, UUID> {
    Images findByName(String name);
}
