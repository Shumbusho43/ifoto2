package zexos.com.ifoto.ifoto.enums;

public enum IUserRoles {
    ROLE_USER,
    ROLE_ADMIN
}
