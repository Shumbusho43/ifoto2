package zexos.com.ifoto.ifoto.enums;

public enum ErrorMessages {
    MISSING_REQUIRED_FIELD("Missing required field"),
    INVALID_EMAIL_FORMAT("Invalid email format"),
    RECORD_EXISTS("This email already exists,try another one"),
    INVALID_ACTIVATION_CODE("Invalid activation code"),
    PASSWORD_NOT_MATCH("Password not match"),
    USER_NOT_FOUND("User not found"),
    INVALID_CREDENTIALS("Invalid email or password");

    private String message;

    ErrorMessages(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
    String errorMessage;

    public String getErrorMessage() {
        return errorMessage;
    }
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
