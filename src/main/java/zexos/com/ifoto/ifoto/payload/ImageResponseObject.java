package zexos.com.ifoto.ifoto.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ImageResponseObject {
    private String name;
    private String url;
    private String type;
    private int size;
}
