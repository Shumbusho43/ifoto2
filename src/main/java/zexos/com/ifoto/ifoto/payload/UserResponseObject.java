package zexos.com.ifoto.ifoto.payload;

import lombok.Data;
import zexos.com.ifoto.ifoto.models.Roles;

import java.util.Collection;

@Data
public class UserResponseObject {
    private String id;
    private String username;
    private String status;
    private Boolean verified;
    private String createdAt;
    private String activatedAt;
    private Collection<Roles> roles;
}