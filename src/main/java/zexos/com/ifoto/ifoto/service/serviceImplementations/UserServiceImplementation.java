package zexos.com.ifoto.ifoto.service.serviceImplementations;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import zexos.com.ifoto.ifoto.enums.ErrorMessages;
import zexos.com.ifoto.ifoto.exceptions.BadRequestException;
import zexos.com.ifoto.ifoto.models.AppUser;
import zexos.com.ifoto.ifoto.models.Roles;
import zexos.com.ifoto.ifoto.repository.RoleRepository;
import zexos.com.ifoto.ifoto.repository.UserRepository;
import zexos.com.ifoto.ifoto.service.UserService;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

@Service
@Slf4j
@Transactional
public class UserServiceImplementation implements UserService {
    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;
   private UserRepository userRepository;
   private RoleRepository roleRepository;

    public UserServiceImplementation(UserRepository userRepository, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        AppUser user= userRepository.findByUsername(username);
        if(user==null){
            log.error("User {} not found", username);
            throw new UsernameNotFoundException("User not found");
        }
        log.info("User {} found", username);
        Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
        user.getRoles().forEach(role -> {
            authorities.add(new SimpleGrantedAuthority(role.getName()));
        });
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), authorities);
    }
    @Override
    public AppUser addUser(AppUser user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
       return  userRepository.save(user);
    }

    @Override
    public void addRoleToUser(String username, String role) {
        log.info("Adding role {} to user {}", role, username);
        AppUser user = userRepository.findByUsername(username);
        Roles roles=roleRepository.findByName(role);
        user.getRoles().add(roles);
        userRepository.save(user);
    }

    @Override
    public AppUser getLoggedInUser() {
        String email;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            email = ((UserDetails) principal).getUsername();
        } else {
            email = principal.toString();
        }
        AppUser findByEmail = userRepository.findByUsername(email);
        if (findByEmail == null) {
            throw new BadRequestException(ErrorMessages.USER_NOT_FOUND.getMessage());
        }
        return findByEmail;
    }
    @Override
    public AppUser getUser(String email) {
        AppUser findById = userRepository.findByUsername(email);
        if(findById==null){
            throw new BadRequestException(ErrorMessages.USER_NOT_FOUND.getMessage());
        }
        return findById;
    }

    @Override
    public AppUser uploadProfilePicture(MultipartFile file, UUID id) throws IOException {
        //finding user
        AppUser user = userRepository.findById(id).orElseThrow(()->new BadRequestException("User not found"));
        user.setProfilePicture(file.getBytes());
        return userRepository.save(user);
    }

    @Override
    public Page getUsers(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        return userRepository.findAll(pageable);
    }

    @Override
    public void deleteUser(AppUser user) {
        userRepository.delete(user);
    }

}
