package zexos.com.ifoto.ifoto.service;

import zexos.com.ifoto.ifoto.models.Roles;

public interface RoleService {
    Roles saveRole(Roles role);
    Roles getRole(String name);
}
