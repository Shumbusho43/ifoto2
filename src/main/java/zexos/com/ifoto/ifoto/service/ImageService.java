package zexos.com.ifoto.ifoto.service;

import org.springframework.web.multipart.MultipartFile;
import zexos.com.ifoto.ifoto.models.Images;

import java.io.IOException;
import java.util.UUID;
import java.util.stream.Stream;

public interface ImageService{
    Images store(MultipartFile file, UUID userId) throws IOException;
    Images  getImage(UUID id);
    Stream<Images> getAllImages();
}
