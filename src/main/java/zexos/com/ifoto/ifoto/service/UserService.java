package zexos.com.ifoto.ifoto.service;

import org.springframework.data.domain.Page;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.multipart.MultipartFile;
import zexos.com.ifoto.ifoto.models.AppUser;

import java.io.IOException;
import java.util.UUID;

public interface UserService extends UserDetailsService {
    AppUser addUser(AppUser user);
    void addRoleToUser(String username, String role);
    AppUser getLoggedInUser();
    AppUser getUser(String id);
    AppUser uploadProfilePicture(MultipartFile file, UUID id) throws IOException;
    Page getUsers(int page, int size);
    void deleteUser(AppUser user);
}
