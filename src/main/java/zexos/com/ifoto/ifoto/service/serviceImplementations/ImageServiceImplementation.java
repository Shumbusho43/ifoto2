package zexos.com.ifoto.ifoto.service.serviceImplementations;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import zexos.com.ifoto.ifoto.exceptions.BadRequestException;
import zexos.com.ifoto.ifoto.models.AppUser;
import zexos.com.ifoto.ifoto.models.Images;
import zexos.com.ifoto.ifoto.repository.ImageRepostory;
import zexos.com.ifoto.ifoto.repository.UserRepository;
import zexos.com.ifoto.ifoto.service.ImageService;
import zexos.com.ifoto.ifoto.utils.CalculateCurrentTime;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

@Service
@AllArgsConstructor
@Transactional
public class ImageServiceImplementation implements ImageService {
    private final ImageRepostory imageRepostory;
    private final UserRepository userRepository;
    @Override
    public Images store(MultipartFile file,UUID userId) throws IOException {
        //finding user
        AppUser user = userRepository.findById(userId).orElseThrow(()->new BadRequestException("User not found"));
        String fileName= StringUtils.cleanPath(file.getOriginalFilename());
        //checking if file name already in
        Images image=imageRepostory.findByName(fileName);
        if(image!=null){
            throw new BadRequestException("Image already exists");
        }
        //current date
        String current_date= CalculateCurrentTime.getCurrentTimeUsingDate("yyyy-MM-dd");
        //checking count of images a user has
        List<Images> userImages=imageRepostory.findAll();
        //looping through images
        int count=0;
        for(Images image1:userImages){
            if(image1.getUser().getId().equals(userId) && image1.getCreated().equals(current_date)){
                count++;
            }
        }
        if(count>=3){
            throw new BadRequestException("You can't upload more than 3 images per day");
        }
        String type=file.getContentType();
        Images images=new Images();
        images.setName(fileName);
        images.setType(type);
        images.setData(file.getBytes());
        images.setUser(user);
        return imageRepostory.save(images);
    }

    @Override
    public Images getImage(UUID id) {
        //Getting an image
        return imageRepostory.findById(id).orElse(null);
    }

    @Override
    public Stream<Images> getAllImages() {
        return imageRepostory.findAll().stream();
    }
}
