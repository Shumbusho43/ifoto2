package zexos.com.ifoto.ifoto.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class MailService {

    @Autowired
    private JavaMailSender mailSender;


    public void sendOptCode(String toEmail,String opt){
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("davidshumbusho10@gmail.com");
        message.setTo(toEmail);
        message.setText("Hi !\n" +
                "\n" +
                "Your verification code is "+opt+". \n" +
                "\n" +
                "This code expires in 5 minutes.\n" +
                "\n" +
                "If you have any questions, send us an email davidshumbusho10@gmail.com.\n" +
                "\n" +
                "We’re glad you’re here!\n" +
                "\n");
        message.setSubject("Ifoto Verification code");
        mailSender.send(message);
    }
    public void sendChangeEmailMail(String toEmail,String opt){
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("davidshumbusho10@gmail.com");
        message.setTo(toEmail);
        message.setText("Hi !\n" +
                "\n" +
                "Your verification code is "+opt+". \n" +
                "\n" +
                "This code expires in 5 minutes.\n" +
                "\n" +
                "If you have any questions, send us an email davidshumbusho10@gmail.com.\n" +
                "\n" +
                "We’re glad you’re here!\n" +
                "\n");
        message.setSubject("Ifoto Verification code");
        mailSender.send(message);
    }

    public void sendResetPasswordMail(String toEmail,String activationCodes){
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("davidshumbusho10@gmail.com");
        message.setTo(toEmail);
        message.setText("Hi !\n" +
                "\n" +
                "You've requested to reset password to ifoto platform, " +
                "your verification code is "+activationCodes+". \n" +
                "\n" +
                "This code expires in 5 minutes.\n" +
                "\n" +
                "If you have any questions, send us an email davidshumbusho10@gmail.com.\n" +
                "\n" +
                "We’re glad you’re here!\n" +
                "\n");
        message.setSubject("Ifoto Verification code");
        mailSender.send(message);
    }

}

