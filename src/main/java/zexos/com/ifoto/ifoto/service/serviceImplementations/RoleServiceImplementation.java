package zexos.com.ifoto.ifoto.service.serviceImplementations;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import zexos.com.ifoto.ifoto.models.Roles;
import zexos.com.ifoto.ifoto.repository.RoleRepository;
import zexos.com.ifoto.ifoto.service.RoleService;

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class RoleServiceImplementation implements RoleService {
    private final RoleRepository roleRepository;

    @Override
    public Roles saveRole(Roles roles) {
        log.info("Saving role: {}", roles);
        return roleRepository.save(roles);
    }

    @Override
    public Roles getRole(String name) {
        return roleRepository.findByName(name);
    }

}
