package zexos.com.ifoto.ifoto.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

import static javax.persistence.FetchType.EAGER;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name="users",uniqueConstraints ={@UniqueConstraint(columnNames={"username"})} )
@Data
public class AppUser {
    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;
    @Email
    @NotBlank
    private String username;
    @NotBlank
    private String password;
    @Lob
    private byte[] profilePicture;
    private String activationCode;
    private String status="PENDING";
    private boolean verified;
    private String createdAt;
    private String activatedAt;
    @ManyToMany(fetch =EAGER)
    private Collection<Roles> roles=new ArrayList<>();

    public AppUser(String username, String password, boolean verified,String activatedAt) {
        this.username = username;
        this.password = password;
        this.verified = verified;
        this.activatedAt=activatedAt;
    }
}
