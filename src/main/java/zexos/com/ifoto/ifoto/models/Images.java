package zexos.com.ifoto.ifoto.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import zexos.com.ifoto.ifoto.utils.CalculateCurrentTime;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Images {
    @Id
    @GeneratedValue(strategy = javax.persistence.GenerationType.AUTO)
    private UUID id;
    @OneToOne
    AppUser user;
    private String name;
    private String type;
    @Lob
    private byte[] data;
    private String created= CalculateCurrentTime.getCurrentTimeUsingDate("yyyy-MM-dd");
}
