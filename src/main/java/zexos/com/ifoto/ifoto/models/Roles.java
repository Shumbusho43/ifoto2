package zexos.com.ifoto.ifoto.models;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.UUID;
@Entity
@Data
public class Roles {
    @Id
    @GeneratedValue(strategy = javax.persistence.GenerationType.AUTO)
    private UUID id;
    private String name;
    Roles (){}
    public Roles(String name,UUID id){
        this.name = name;
        this.id=id;
    }
}
