package zexos.com.ifoto.ifoto.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class CalculateCurrentTime {
   public static String getCurrentTimeUsingDate(String format) {
       DateTimeFormatter dtf = DateTimeFormatter.ofPattern(format);
//       DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
       LocalDateTime now = LocalDateTime.now();
       String result=dtf.format(now);
       return result;
    }
}
