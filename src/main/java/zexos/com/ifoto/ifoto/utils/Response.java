package zexos.com.ifoto.ifoto.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import java.time.ZonedDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Response {
    private String message;
    private boolean success;
    private Object data;
    private ZonedDateTime timestamp;
    public Response(boolean success, String message,ZonedDateTime timestamp) {
        this.success = success;
        this.message = message;
        this.timestamp = ZonedDateTime.now();
    }
    public Response(boolean success, String message) {
        this.success = success;
        this.message = message;
    }
    public Response(boolean success, String message,Object data,ZonedDateTime timestamp) {
        this.success = success;
        this.message = message;
        this.data=data;
        this.timestamp = timestamp;
    }
}
